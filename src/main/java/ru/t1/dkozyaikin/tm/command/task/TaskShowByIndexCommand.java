package ru.t1.dkozyaikin.tm.command.task;

import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    public static final String NAME ="task-show-by-index";

    public static final String DESCRIPTION = "Show task by index.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getAuthService().getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
