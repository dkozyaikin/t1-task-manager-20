package ru.t1.dkozyaikin.tm.command.project;

import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-by-index";

    public static final String DESCRIPTION = "Change status of the PROJECT by index";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE STATUS OF THE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getAuthService().getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, status);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
