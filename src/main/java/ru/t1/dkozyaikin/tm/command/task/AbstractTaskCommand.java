package ru.t1.dkozyaikin.tm.command.task;

import ru.t1.dkozyaikin.tm.api.service.IProjectTaskService;
import ru.t1.dkozyaikin.tm.api.service.ITaskService;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void showTask(Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId() + ";");
        System.out.println("Name: " + task.getName() + ";");
        System.out.println("Description: " + task.getDescription() + ";");
        System.out.println("Status: " + Status.toName(task.getStatus()));
        System.out.println("Created: " + task.getCreated());
        System.out.println("Project ID: " + task.getProjectId());
    }

}
