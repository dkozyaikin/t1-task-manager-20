package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(String userId, String name) {
        Project project = new Project(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(String userId, String name, String description) {
        Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(String userId, String name, String description, Status status) {
        Project project = new Project(name, description, status);
        project.setUserId(userId);
        return add(project);
    }

}

