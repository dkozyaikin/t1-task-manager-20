package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> result = findAll(userId);
        models.removeAll(result);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        List<M> result = new ArrayList<>();
        for (M model : models) {
            System.out.println(model.getId());
            if (userId.equals(model.getUserId())) result.add(model);
        }
        return result;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        List<M> result = findAll(userId);
        if (comparator == null) return result;
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        for (M model : models) {
            if (!id.equals(model.getId())) continue;
            if (!userId.equals(model.getUserId())) continue;
            return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public Integer getSize(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        int count = 0;
        for (final M model : models) {
            if (userId.equals(model.getUserId())) count++;
        }
        return count;
    }

    @Override
    public M removeById(String userId, String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(String userId, Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M add(String userId, M model) {
        if (userId == null || userId.isEmpty() || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M remove(String userId, M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

}
