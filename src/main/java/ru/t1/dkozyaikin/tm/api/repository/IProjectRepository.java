package ru.t1.dkozyaikin.tm.api.repository;

import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Status status);

}
