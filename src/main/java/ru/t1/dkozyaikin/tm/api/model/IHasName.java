package ru.t1.dkozyaikin.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
