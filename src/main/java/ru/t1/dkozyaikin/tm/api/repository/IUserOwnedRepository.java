package ru.t1.dkozyaikin.tm.api.repository;

import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId) throws AbstractException;

    boolean existsById(String userId, String id) throws AbstractException;

    List<M> findAll(String userId) throws AbstractException;

    List<M> findAll(String userId, Comparator comparator) throws AbstractException;

    M findOneById(String userId, String id) throws AbstractException;

    M findOneByIndex(String userId, Integer index) throws AbstractException;

    Integer getSize(String userId) throws AbstractException;

    M removeById(String userId, String id) throws AbstractException;

    M removeByIndex(String userId, Integer index) throws AbstractException;

    M add(String userId, M model) throws AbstractException;

    M remove(String userId, M model) throws AbstractException;

}
