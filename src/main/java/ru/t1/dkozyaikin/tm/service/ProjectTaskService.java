package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.service.IProjectTaskService;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkozyaikin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dkozyaikin.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozyaikin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.UserIdEmptyException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String taskId, final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        List<Task> tasksToRemove = taskRepository.findAllTasksByProjectId(userId, project.getId());
        for (Task task : tasksToRemove) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public void removeProjectByIndex(final String userId, Integer projectIndex) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectIndex == null || projectIndex < 0 ) throw new IndexIncorrectException();
        if (projectIndex >= projectRepository.getSize()) throw new IndexIncorrectException();
        final Project project = projectRepository.findOneByIndex(userId, projectIndex);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllTasksByProjectId(userId, project.getId());
        for (Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeByIndex(userId, projectIndex);
    }

    @Override
    public void clearAllProjects(final String userId) throws AbstractException{
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<Project> projects = projectRepository.findAll(userId);
        final List<Task> tasks = taskRepository.findAll(userId);
        for (final Project project: projects) {
            for (final Task task : tasks) {
                if (project.getId().equals(task.getProjectId())) taskRepository.removeById(userId, task.getId());
            }
        }
        projectRepository.clear(userId);
    }

    @Override
    public void unbindTaskFromProject(String userId, final String taskId, final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
