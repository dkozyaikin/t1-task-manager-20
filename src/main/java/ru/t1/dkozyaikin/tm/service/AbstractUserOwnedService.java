package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozyaikin.tm.api.service.IUserOwnedService;
import ru.t1.dkozyaikin.tm.enumerated.Sort;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.field.IdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozyaikin.tm.exception.field.UserIdEmptyException;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public void clear(final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @Override
    public List<M> findAll(final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) throws AbstractException {
        if (sort == null) return repository.findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public M findOneById(final String userId, final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id ==null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public Integer getSize(final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize();
    }

    @Override
    public M removeById(final String userId, final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id ==null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Override
    public M add(final String userId, final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    public M remove(final String userId, final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.remove(userId, model);
    }

}
